﻿<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>P4Python</Name>
  <Location>/Engine/Extras/Maya_AnimationRiggingTools/MayaTools/General/Scripts</Location>
  <Date>2016-03-23T13:00:28.1663853-04:00</Date>
  <Function>adds python bindings for perforce</Function>
  <Justification>so we can automate tasks for artists, like auto-syncing the files they need on file open, checking for latest, etc. I cannot find a EULA online, but the following text is included directly in the python file,which I've pasted here: http://pastebin.com/prYGpqPe</Justification>
  <Platforms>
    <Platform>PC</Platform>
  </Platforms>
  <Products>
    <Product>UDK4</Product>
    <Product>UE4</Product>
  </Products>
  <TpsType>Source Code</TpsType>
  <Eula />
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <Redistribute>false</Redistribute>
  <IsSourceAvailable>true</IsSourceAvailable>
  <NoticeType>None</NoticeType>
  <Notification />
  <LicenseFolder />
</TpsData>